#include "malloc.h"
#include <stdio.h>

/*
**	libft utils
*/
void	ft_bzero(void *s, size_t n)
{
	while (n--)
		*(unsigned char *)s++ = '\0';
}

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char *pb;

	pb = b;
	while (len--)
	{
		*pb = (unsigned char)c;
		pb++;
	}
	return (b);
}

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char		*cdst;
	const char	*csrc;

	cdst = (char *)dst;
	csrc = (const char *)src;
	while (n--)
	{
		*cdst++ = *csrc++;
	}
	return (dst);
}
/*
**	POOL TOOLS
*/
pinfo determine_pool_by_block_size(size_t size)
{
	if (size <= T_BLOC_SIZE)
		return (pinfo){.size = T_POOL_SIZE, .type = TINY};
	if (size <= S_BLOC_SIZE)
		return (pinfo){.size = S_POOL_SIZE, .type = SMALL};
	return (pinfo){.size = size + sizeof(pool), .type = LARGE};
}

/*
**	BLOCK TOOLS
*/

size_t block_size_by_type(enum e_ptype type)
{
	if (type == TINY) {
		return T_BLOC_SIZE;
	} else if (type == SMALL) {
		return S_BLOC_SIZE;
	}
	return 0;
}

enum e_ptype pool_type_by_bloc_size(size_t size)
{
	if (size > S_BLOC_SIZE) {
		return LARGE;
	} else if (size > T_BLOC_SIZE) {
		return SMALL;
	}
	return TINY;
}

size_t get_boundary_size(size_t size)
{
	if (size > S_BLOC_SIZE) {
		return size;
	} else if (size > T_BLOC_SIZE) {
		return S_BLOC_SIZE;
	}
	return T_BLOC_SIZE;
}

/*
**	FREE TOOLS
*/
char addr_is_valid(char *addr)
{
	pool *heap = last_pool;
	while (heap)
	{
		if ((addr > (char *)(heap + 1) && addr < (char*)(heap + 1) + heap->size))
		{
			break;
		}
		heap = heap->next;
	}

	if (heap)
	{
		block *bloc = (block*)(heap + 1);
		size_t typesize = block_size_by_type(bloc->type);
		while ((char*)bloc <= (char*)heap->last_block)
		{
			char *user_ptr = (char*)(bloc + 1);
			if (user_ptr == addr)
				return 1;
			bloc = (block*)((char*)bloc + typesize);
		}
	}
	return 0;
}

bool addr_is_freed(char *addr)
{
  block *bloc = (block*)addr - 1;

  return bloc->freed;
}


/*
** Show memory
*/

/*
 * TINY : [addr of the pool]
 * [begin block addr] - [end block addr] : [size - header] octets
 * [begin block addr] - [end block addr] : [size - header] octets
 * SMALL : [addr of the pool]
 * [begin block addr] - [end block addr] : [size - header] octets
 * [begin block addr] - [end block addr] : [size - header] octets
 * LARGE : [addr of the pool]
 * [begin block addr] - [end block addr] : [size - header] octets
 * Total : [sum of all sizes] octets
 *
*/
void display_pool_infos(pool *p)
{
	if (p->type == TINY)
		printf("TINY : ");
	else if (p->type == SMALL)
		printf("SMALL : ");
	else
		printf("LARGE : ");
	printf("%p\n", p);
}

void display_blocks(pool *p, size_t *total)
{
	block *b = (block*)(p + 1);
	while (b <= p->last_block)
	{
		if (b->freed == true)
			printf("[FREED] ");
		printf("%p - %p : %lu octets\n", b + 1, (char*)b + b->size, b->size - sizeof(block));
		*total += b->size - sizeof(block);
		if (p->type == LARGE)
			break;
		b = (block*)((char*)b + block_size_by_type(p->type));
	}
}

void show_alloc_mem()
{
	pool *p = last_pool;
	size_t total_octets = 0;
	while (p)
	{
		display_pool_infos(p);
		display_blocks(p, &total_octets);
		p = p->next;
	}
	printf("Total : %lu octets\n", total_octets);
}

void process_to_hexdump(const void* data, size_t size) {
	char ascii[17];
	size_t i, j;
	ascii[16] = '\0';
	for (i = 0; i < size; ++i) {
		printf("%02X ", ((unsigned char*)data)[i]);
		if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			printf(" ");
			if ((i+1) % 16 == 0) {
				printf("|  %s \n", ascii);
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					printf(" ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					printf("   ");
				}
				printf("|  %s \n", ascii);
			}
		}
	}
}

void show_alloc_mem_ex()
{
	pool *p = last_pool;
	int unused;
	(void)unused;
	while (p)
	{
		printf("Pool starting at %p :\n", p);
		process_to_hexdump(p, (char*)p->last_block - (char*)p);
		unused = write(1, "\n", 1);
		p = p->next;
	}
}

