#include "malloc.h"
#include <fcntl.h>
#include <sys/mman.h>

pool *create_pool(pinfo pool_infos)
{
	pool *ret = NULL;
	ret = (pool*)mmap(0, pool_infos.size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);	
	if (ret == MAP_FAILED)
		return NULL;
	ret->free_size = pool_infos.size - sizeof(pool);
	ret->size = ret->free_size;
	ret->type = pool_infos.type;
	ret->last_block = NULL;
	ret->next = NULL;
	return ret;
}

pool *get_pool(size_t size)
{
	pool *cur_pool= NULL;

	pinfo pool_infos = determine_pool_by_block_size(size);
	if (pool_infos.type != LARGE && last_pool)
	{
		cur_pool = last_pool;
		do
		{
			if (cur_pool->type == pool_infos.type && cur_pool->free_size > size)
				break;
			cur_pool = cur_pool->next;
		}
		while (cur_pool);
	}
	
	if (!cur_pool)
	{
		cur_pool = create_pool(pool_infos);
		if (last_pool)
			cur_pool->next = last_pool;
		last_pool = cur_pool;
	}
	return cur_pool;
}
