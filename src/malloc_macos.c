#include "malloc.h"

size_t malloc_size(const void *ptr)
{
	if (!addr_is_valid((char*)ptr))
		return 0;
	block * b = (block*)ptr - 1;
	return b->size - sizeof(block);
}

size_t malloc_good_size(size_t size)
{
	size = (size & ~(size_t)0x0F) + 16;
	if (!size)
	{
		size = (1 & ~(size_t)0x0F) + 16;
	}
	return size;
}
