#include "malloc.h"

pthread_mutex_t gMutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

void *aloc_block(size_t size)
{
	block *res = NULL;
	pool *p = NULL;

	if ((p = get_pool(size)))
	{
		if (p->last_block)
		{
			res = (block*)((char*)p->last_block + block_size_by_type(p->type));
		}
		else
		{
			res = (block*)((char*)p + sizeof(pool));
		}
		res->size = size;
		res->type = p->type;
		res->freed = false;
		p->last_block = res;
		p->free_size -= size;
	}
	return res;
}

block *get_freed_block(size_t size)
{
	free_header *free_bloc = free_bin[pool_type_by_bloc_size(size)];

	if (free_bloc)
	{
		free_header *cur_freed_bloc = free_bloc;
		do
		{
			if (cur_freed_bloc->size >= size)
			{
				if (cur_freed_bloc->prev)
					cur_freed_bloc->prev->next = cur_freed_bloc->next;
				if (cur_freed_bloc->next)
					cur_freed_bloc->next->prev = cur_freed_bloc->prev;
				if (cur_freed_bloc == free_bloc)
					free_bin[pool_type_by_bloc_size(size)] = cur_freed_bloc->prev;
				return (block*)free_bloc;
			}
		}
		while ((free_bloc = free_bloc->prev));
	}
	return NULL;
}

void *get_avalaible_block(size_t size)
{
	if (size > S_BLOC_SIZE)
		return NULL;
	block *bloc = get_freed_block(size);
	if (bloc)
	{
		bloc->freed = false;
	}
	return bloc;
}

void *start_malloc(size_t size)
{
	void *res = NULL;

	size = (size & ~(size_t)0x0F) + 16;
	if (!size)
	{
		size = (1 & ~(size_t)0x0F) + 16;
	}
	size += sizeof(block);
	size_t real_size = get_boundary_size(size);
	
	if (!(res = get_avalaible_block(real_size)))
	{
		res = aloc_block(real_size);
	}
	return res ? res + sizeof(block) : res;
}

void *malloc(size_t size)
{
	void *res = NULL;
	pthread_mutex_lock(&gMutex);
	res = start_malloc(size);
	pthread_mutex_unlock(&gMutex);
	return res;
}
