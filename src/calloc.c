#include "malloc.h"

void *calloc(size_t nmemb, size_t size)
{
	void *ret = NULL;
	
	if (!size || !nmemb)
		size = nmemb = 1;
	size_t s = size * nmemb;
	pthread_mutex_lock(&gMutex);
	if (!s)
		return NULL;
	ret = malloc(s);
	ft_bzero(ret, s);
	pthread_mutex_unlock(&gMutex);
	return ret;
}
