#include "malloc.h"

void *start_realloc(void *ptr, size_t size)
{
	void *ret = NULL;

	if (ptr == NULL)
		return malloc(size);
	if (!addr_is_valid((char*)ptr))
		return NULL;

	block *bloc = ptr - sizeof(block);

	size_t new_size = (size & ~(size_t)0x0F) + 16;
	if (!size)
	{
		size = (1 & ~(size_t)0x0F) + 16;
	}
	size += sizeof(block);
	size_t shared_size = new_size < (bloc->size - sizeof(block)) ? new_size : (bloc->size - sizeof(block));
	ret = malloc(new_size);
	ft_memcpy(ret, ptr, shared_size);
	free(ptr);
	return ret;
}

void *realloc(void *ptr, size_t size)
{
	void *res = NULL;

	pthread_mutex_lock(&gMutex);
	res = start_realloc(ptr, size);
	pthread_mutex_unlock(&gMutex);
	return res;
}
