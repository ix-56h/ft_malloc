#include "malloc.h"
#include <sys/mman.h>
#include <stdio.h>

free_header *free_bin[2] = {NULL, NULL};
pool *last_pool;

void free_large(block *head)
{
	pool *prev = NULL;
	pool *cur = last_pool;
	pool *head_pool = (pool*)head - 1;
	if (cur)
	{
		while (cur && cur != head_pool)
		{
			prev = cur;
			cur = cur->next;
		}
	}
	if (cur)
	{
		if (head_pool == last_pool)
			last_pool = head_pool->next; 
		else
			prev->next = head_pool->next;
	}
	munmap(head_pool, head_pool->size + sizeof(pool));


}

void append_free(block *head, enum e_ptype type)
{
	free_header *cur_free = (free_header*)head;
	cur_free->freed = true;
	cur_free->next = NULL;
	cur_free->prev = NULL;
	
	if (free_bin[type])
	{
		free_header *last_free = free_bin[type];

		cur_free->prev = last_free;
		last_free->next = cur_free;
		cur_free->next = NULL;
	}
	free_bin[type] = cur_free;
}

void start_free(void *ptr)
{
	if (!addr_is_valid((char*)ptr))
	{
		return;
	}
	if (addr_is_freed((char*)ptr)) {
		return;
	}
	block *bloc = ptr - sizeof(block);
	
	if (LARGE == bloc->type)
	{
		free_large(bloc);
		return ;
	}
	append_free(bloc, pool_type_by_bloc_size(bloc->size));
}

void free(void *ptr)
{
	pthread_mutex_lock(&gMutex);
	if (!ptr)
		return;
	start_free(ptr);
	pthread_mutex_unlock(&gMutex);
}
