#ifndef MALLOC_H
# define MALLOC_H

# include <stddef.h>
# include <unistd.h>

# define __USE_GNU	1
# include <pthread.h>

# include <string.h>

#define PAGE_SZ (size_t)getpagesize()

#define T_POOL_SIZE PAGE_SZ * 4
#define S_POOL_SIZE PAGE_SZ * 16
#define NB_BLOCS_PER_POOL 128
#define T_BLOC_SIZE (((T_POOL_SIZE - sizeof(pool)) / NB_BLOCS_PER_POOL) & ~(size_t)0x0F) + 16
#define S_BLOC_SIZE (((S_POOL_SIZE - sizeof(pool)) / NB_BLOCS_PER_POOL) & ~(size_t)0x0F) + 16

// Mutex implementation for thread safe
extern pthread_mutexattr_t 	gAttr;
extern pthread_mutex_t	gMutex;

typedef enum e_bool
{
	false,
	true
}			bool;

// Generic type for pools and blocks
enum e_ptype {
	TINY,
	SMALL,
	LARGE
};

// Struct containing temporary informations of the pool
typedef struct pool_infos
{
	size_t size;
	enum e_ptype type;
}				pinfo;

// Linked list of blocks
typedef struct s_block
{
	char		__pad1[16];
	size_t		size;
	enum e_ptype	type;
	bool	freed;
}				block;

// Linked link of pools
typedef struct	s_pool
{
	struct s_pool *next;
	block *last_block;
	size_t	size;
	size_t	free_size;
	enum e_ptype type;
	char __pad[28];
}				pool;

// Struct for casting blocks and reinterpret __pad1 16 bits as pointers for the prev and next freed block
typedef struct s_free_header
{
	struct s_free_header *prev;
	struct s_free_header *next;
	size_t	size;
	enum e_ptype	type;
	bool	freed;
}				free_header;

/*
**	malloc_macos.c
*/
size_t malloc_size(const void *ptr);
size_t malloc_good_size(size_t size);

/*
**  malloc.c functions
*/
void *malloc(size_t size);

/*
**  free.c functions
*/
void free(void *ptr);

/*
**  realloc.c functions
*/
void *realloc(void *ptr, size_t size);

/*
**  calloc.c functions
*/
void *calloc(size_t nmemb, size_t size);

/*
**  tools.c functions
*/
// generic 
void	ft_bzero(void *s, size_t n);
void	*ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);

// local
pinfo determine_pool_by_block_size(size_t size);
size_t block_size_by_type(enum e_ptype type);
size_t get_boundary_size(size_t size);
enum e_ptype pool_type_by_bloc_size(size_t size);
char addr_is_valid(char *addr);
bool addr_is_freed(char *addr);
void show_alloc_mem();
void show_alloc_mem_ex();

/*
**  pool.c functions
*/
pool *get_pool(size_t size);

extern free_header *free_bin[2];
extern pool *last_pool;

//# define _DEBUG
/*

#define LOCATION "%s:%s:%u"
#define ENTERED_LOCATION do { \
char __private__buffer[256]; \
size_t size = snprintf(__private__buffer, sizeof(__private__buffer), "Entered "LOCATION"\n", __FILE__, __func__, __LINE__); \
write(1, __private__buffer, size);\
} while (0);
#define EXITED_LOCATION do { \
char __private__buffer[256]; \
size_t size = snprintf(__private__buffer, sizeof(__private__buffer), "Exited "LOCATION"\n", __FILE__, __func__, __LINE__); \
write(1, __private__buffer, size);\
} while (0);
*/
#endif


