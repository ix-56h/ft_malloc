#include "malloc.h"
#include <bsd/stdlib.h>

void hardrand()
{

	char	*arr[8192];
	size_t	arr_sz = 0;
	size_t	cur_sz;

	for (int i = 0; i < 8192; i++) {

		if (arc4random() % 2) {

			cur_sz = arc4random() % 10232;
			arr[arr_sz] = (char*)malloc(cur_sz);

			// fill allocated mem with 0xff
			ft_memset(arr[arr_sz], -1, cur_sz);
			arr_sz++;

		}
		// if something to free
		else if (arr_sz > 0 && arc4random() % 2) {

			size_t rd_idx = arc4random() % arr_sz;
			free(arr[rd_idx]);

			arr[rd_idx] = arr[arr_sz - 1];
			arr_sz--;
		}
	}
	for (int i = 0; i < arr_sz; i++) {
		free(arr[i]);
	}



}

int main(int ac, char **av) {
	hardrand();
	//show_alloc_mem();
	show_alloc_mem_ex();
}
