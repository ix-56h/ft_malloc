# Executable's name (Can be changed)
NAME		= libft_malloc.so

# All the directories needed to know where files should be (Can be changed)
OBJDIR		= obj/
SRCDIR		= src/
INCDIR		= inc/

# Source files (Can be changed)
SRC		= \
		  malloc.c \
		  free.c \
		  realloc.c \
		  pool.c \
		  calloc.c \
		  tools.c \
		  malloc_macos.c

# Some tricks in order to get the makefile doing his job the way I want (Can't be changed)
CSRC		= $(addprefix $(SRCDIR), $(SRC))
COBJ		= $(addprefix $(OBJDIR), $(OBJ))
INCLUDES	= $(foreach include, $(INCDIR), -I$(include))

# How files should be compiled with set flags (Can be changed)
CC			= gcc
OBJ			= $(SRC:.c=.o)
CFLAGS		= $(INCLUDES) -Wall -Wextra -Werror -fPIC -O3 -flto

# Color codes
WHITE = \033[1;37m
CYAN = \033[36m
GREEN = \033[1m\033[32m
GRED = \033[30;41m
WRED = \033[96;41m
BRED = \033[0;31;40m
BBLUE = \033[0;36m
BBBLUE = \033[1;36m
WBLUE = \033[31;44m
YELLOW = \033[33m
CLEAR = \033[0;0m



# Check if object directory exists, build libft and then the Project

all: $(NAME)

$(NAME): $(OBJDIR) $(COBJ)
	@printf "$(YELLOW)\n      - Building $(CLEAR)$(NAME) $(YELLOW)...\n$(CLEAR)"
	$(CC) -shared $(COBJ) -o $(NAME)
	@printf "$(GREEN)***   Project $(NAME) successfully compiled   ***\n$(CLEAR)"

$(OBJDIR):
	@mkdir -p $(OBJDIR)

# Redefinition of implicit compilation rule to prompt some colors and file names during the said compilation

$(OBJDIR)%.o: $(SRCDIR)%.c
	@printf "$(GREEN)Compiling ...\n$(CLEAR)"
	@$(CC) $(CFLAGS) -c $< -o $@

# Deleting all .o files and then the directory where they were located

clean:
	@printf "$(GREEN)***   Deleting all object from $(NAME)   ...   ***\n$(CLEAR)"
	@rm -f $(COBJ)

# Deleting the executable after cleaning up all .o files

fclean: clean
	@printf "$(GREEN)***   Deleting executable file from $(NAME)   ...   ***\n$(CLEAR)"
	@rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
