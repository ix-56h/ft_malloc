#!/bin/bash

export LD_LIBRARY_PATH="."
export LD_PRELOAD="${PWD}/libft_malloc.so"
export LD_FORCE_FLAT_NAMESPACE=1
$@
if [[ $? -ge 129 ]]
then
	echo "[$?] on $@"
	exit 314
else
	echo "OK"
fi
